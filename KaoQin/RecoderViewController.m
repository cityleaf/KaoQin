//
//  RecoderViewController.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "RecoderViewController.h"
#import "RecordCell.h"
#import "LYTools.h"

@interface RecoderViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray  *_chuArr;    //出勤数据源
    NSMutableArray  *_unchuArr;  //未出勤数据源
    
    UIScrollView    *_bigScrollView;
    UITableView     *_chuVC;
    UITableView     *_unchuVC;
    
    UISegmentedControl      *_segment;
}
@end

@implementation RecoderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = [UIColor redColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [LYTools isLoginWithSuccess:^{
        [self steupData];
        [self steupUI];
    } failure:^{
        [self unLoginUI];
    }];
}
- (void)steupData
{
    _chuArr = [NSMutableArray array];
    _unchuArr = [NSMutableArray array];
    [self requestForData];
    
}
- (void)requestForData{
    NSMutableDictionary     *json = [LYTools getJsonFrompath:@"unchu.geojson"];
    

    NSArray *unResponse = json[@"response"];
   
    for (NSMutableDictionary *dic in unResponse) {
        RecoderModel *unchuModel = [RecoderModel allocWithDic:dic];
        [_unchuArr addObject:unchuModel];
    }
    

    [_unchuVC reloadData];
    RecoderModel    *chuModel0 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel1 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel2 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel3 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel4 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel5 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel6 = [[RecoderModel alloc]init];
    RecoderModel    *chuModel7 = [[RecoderModel alloc]init];
    chuModel0.type = @"0";
    chuModel0.className = @"计算机组成原理";
    chuModel0.collegeType = @"0";
    chuModel0.time = @"2016-3-14";
    
    chuModel1.type = @"0";
    chuModel1.className = @"C语言程序设计";
    chuModel1.collegeType = @"0";
    chuModel1.time = @"2016-3-14";
    
    chuModel2.type = @"0";
    chuModel2.className = @"数据结构";
    chuModel2.collegeType = @"0";
    chuModel2.time = @"2016-3-15";
    
    chuModel3.type = @"0";
    chuModel3.className = @"图形学";
    chuModel3.collegeType = @"0";
    chuModel3.time = @"2016-3-15";
    
    chuModel4.type = @"0";
    chuModel4.className = @"面向对象程序设计";
    chuModel4.collegeType = @"0";
    chuModel4.time = @"2016-3-15";
    
    chuModel5.type = @"0";
    chuModel5.className = @"高等数学";
    chuModel5.collegeType = @"4";
    chuModel5.time = @"2016-3-16";
    
    chuModel6.type = @"0";
    chuModel6.className = @"英语";
    chuModel6.collegeType = @"2";
    chuModel6.time = @"2016-3-17";
    
    chuModel7.type = @"0";
    chuModel7.className = @"马克思主义原理";
    chuModel7.collegeType = @"3";
    chuModel7.time = @"2016-3-17";
    
    [_chuArr addObject:chuModel0];
    [_chuArr addObject:chuModel1];
    [_chuArr addObject:chuModel2];
    [_chuArr addObject:chuModel3];
    [_chuArr addObject:chuModel4];
    [_chuArr addObject:chuModel5];
    [_chuArr addObject:chuModel6];
    [_chuArr addObject:chuModel7];
    [_chuVC reloadData];
    
}
- (void)unLoginUI
{
    self.title = @"出勤记录";
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    lable.text = @"您还没有登录";
    lable.font = [UIFont systemFontOfSize:24];
    lable.textColor = [UIColor lightGrayColor];
    lable.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
    lable.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lable];
}
- (void)steupUI
{
    _segment = [[UISegmentedControl alloc]initWithItems:@[@"出勤",@"未出勤"]];
    _segment.selectedSegmentIndex = 0;
    _segment.frame = CGRectMake(0, 0, 100, 30);
    _segment.tintColor = [UIColor whiteColor];
//    [customNavigationBar addSubview:segment];
    [_segment addTarget:self action:@selector(segmentChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.navigationItem setTitleView:_segment];
    
    NSLog(@"height = %f",HEIGHT);
    _bigScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-49-64)];
    _bigScrollView.contentSize = CGSizeMake(WIDTH*2,HEIGHT-49-64);
    _bigScrollView.tag = 1000;
    _bigScrollView.delegate = self;
    _bigScrollView.pagingEnabled = YES;
//    bigScrollView.bounces = NO;
    [self.view addSubview:_bigScrollView];
    
    _chuVC = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, _bigScrollView.contentSize.height) style:UITableViewStylePlain];
    _chuVC.delegate = self;
    _chuVC.dataSource = self;
    _chuVC.tag = 10;
    [_chuVC registerNib:[UINib nibWithNibName:@"RecordCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [_bigScrollView addSubview:_chuVC];
    _unchuVC = [[UITableView alloc]initWithFrame:CGRectMake(WIDTH, 0, WIDTH, _bigScrollView.contentSize.height) style:UITableViewStylePlain];
    _unchuVC.delegate = self;
    _unchuVC.dataSource = self;
    [_unchuVC registerNib:[UINib nibWithNibName:@"RecordCell" bundle:nil] forCellReuseIdentifier:@"cell2"];
    [_bigScrollView addSubview:_unchuVC];
   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 10) {
        return _chuArr.count;
    }
    return _unchuArr.count;
}
- (UITableViewCell  *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 10) {
        RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        RecoderModel    *model = _chuArr[indexPath.row];
        [cell cellWithObject:model];
        return cell;
    }
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    RecoderModel    *model = _unchuArr[indexPath.row];
    [cell cellWithObject:model];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SIZE(160);
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

- (void)segmentChangeValue:(UISegmentedControl  *)segment
{
    NSInteger   index = segment.selectedSegmentIndex;
    [UIView animateWithDuration:0.2 animations:^{
    _bigScrollView.contentOffset = CGPointMake(index*WIDTH, 0);
    }];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.tag == 1000) {
        _segment.selectedSegmentIndex = _bigScrollView.contentOffset.x/WIDTH;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
