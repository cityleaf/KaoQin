//
//  main.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
