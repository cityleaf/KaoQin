//
//  UITableView+touch.m
//  QinWuBing
//
//  Created by 刘宇 on 16/3/15.
//  Copyright © 2016年 BangNiWuLian. All rights reserved.
//

#import "UITableView+touch.h"
@implementation UITableView (touch)
//
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    static UIEvent *e = nil;
    
    if (e != nil && e == event) {
        e = nil;
        return [super hitTest:point withEvent:event];
    }
    
    e = event;
    
    if (event.type == UIEventTypeTouches) {
        NSSet *touches = [event touchesForView:self];
        UITouch *touch = [touches anyObject];
        if (touch.phase == UITouchPhaseBegan) {
//            NSLog(@"Touches began");
            [[self superview] touchesBegan:touches withEvent:event];
        }else if(touch.phase == UITouchPhaseEnded){
            NSLog(@"Touches Ended");
            
        }else if(touch.phase == UITouchPhaseCancelled){
            NSLog(@"Touches Cancelled");
            
        }else if (touch.phase == UITouchPhaseMoved){
            NSLog(@"Touches Moved");
            
        }
    }
    return [super hitTest:point withEvent:event];
}

@end
