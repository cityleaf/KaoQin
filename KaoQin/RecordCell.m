//
//  RecordCell.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "RecordCell.h"
#import "Masonry.h"

@implementation RecordCell
- (void)cellWithObject:(RecoderModel *)object{
    self.titleLable.text = object.className;
    self.subTitleLable.text = object.time;
    if ([object.type isEqualToString:@"0"]) {
        //出勤
//        self.type.hidden = YES;
    }else if ([object.type isEqualToString:@"1"]){
        //旷课
        self.typeButton.hidden = NO;
        NSLog(@"haha    %p",self.typeButton);
        [self.typeButton setTitle:@"旷课" forState:UIControlStateNormal];
        [self.typeButton setBackgroundImage:[UIImage imageNamed:@"kuangke"] forState:UIControlStateNormal];
    }else{
        //请假
        self.typeButton.hidden = NO;
        NSLog(@"hhhh    %p",self.typeButton);
        [self.typeButton setTitle:@"请假" forState:UIControlStateNormal];
        [self.typeButton setBackgroundImage:[UIImage imageNamed:@"qingjia_bg"] forState:UIControlStateNormal];
    }
    
    if ([object.collegeType isEqualToString:@"0"]) {
        //计算机学院
        self.img.image = [UIImage imageNamed:@"computer"];
    }else if ([object.collegeType isEqualToString:@"1"]){
        //经济学院
        self.img.image = [UIImage imageNamed:@"jingji"];
    }else if ([object.collegeType isEqualToString:@"2"]){
        //外国语学院
        self.img.image = [UIImage imageNamed:@"english"];
    }else if ([object.collegeType isEqualToString:@"3"]){
        //政法学院
        self.img.image = [UIImage imageNamed:@"zhengfa"];
    }else if ([object.collegeType isEqualToString:@"4"]){
        //数学院
        self.img.image = [UIImage imageNamed:@"math"];
    }else{
        //音乐学院
        self.img.image = [UIImage imageNamed:@"music"];
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self addSubview:self.img];
    [self addSubview:self.titleLable];
    [self addSubview:self.subTitleLable];
    [self addSubview:self.typeButton];
    
    
    [_img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(@0);
        make.width.equalTo(_img.mas_height);
    }];
    [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).offset(SIZE(20));
        make.top.equalTo(@(SIZE(10)));
    }];
    [_subTitleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLable.mas_bottom).offset(SIZE(20));
        make.left.equalTo(_titleLable.mas_left);
    }];
    [_typeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@15);
        make.right.equalTo(@(-15));
        make.bottom.equalTo(@(-15));
        make.width.equalTo(_typeButton.mas_height);
    }];
}
- (UIImageView  *)img{
    if (!_img) {
        _img = [[UIImageView alloc]init];
    }
    return _img;
}
- (UILabel  *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc]init];
    }
    return _titleLable;
}
- (UIButton *)typeButton{
    if (!_typeButton) {
        _typeButton = [[UIButton alloc]init];
//        _typeButton.backgroundColor = [UIColor redColor];
    }
    return _typeButton;
}
- (UILabel  *)subTitleLable{
    if (!_subTitleLable) {
        _subTitleLable = [[UILabel alloc]init];
    }
    return _subTitleLable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
