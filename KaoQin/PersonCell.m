//
//  PersonCell.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/5.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "PersonCell.h"
#import "Masonry-master/Masonry/Masonry.h"

@implementation PersonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self addSubview:self.titleLable];
    [self addSubview:self.subLable];
    
    [_titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(@(SIZE(140)));
        make.bottom.equalTo(@0);
    }];
    
    [_subLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLable.mas_right).offset(SIZE(36));
        make.top.equalTo(@0);
        make.right.equalTo(@0);
        make.bottom.equalTo(@0);
    }];
}
- (UILabel  *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc]init];
        _titleLable.textAlignment = NSTextAlignmentRight;
        _titleLable.textColor = [UIColor colorWithRed:0.59 green:0.58 blue:0.56 alpha:1];
        _titleLable.font = [UIFont systemFontOfSize:16];
    }
    return _titleLable;
}
- (UILabel  *)subLable{
    if (!_subLable) {
        _subLable = [[UILabel alloc]init];
        _subLable.numberOfLines = 0;
        _subLable.textColor = [UIColor colorWithRed:0.45 green:0.45 blue:0.45 alpha:1];
        _subLable.font = [UIFont systemFontOfSize:15];
    }
    return _subLable;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
