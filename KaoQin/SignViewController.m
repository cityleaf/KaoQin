//
//  SignViewController.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "SignViewController.h"
#import "LYTools.h"
#import "MBProgressHUD+GR.h"
#import "QRViewController.h"
@interface SignViewController ()

@end

@implementation SignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self steupUI];
}
- (void)unLoginUI
{
    
}
- (void)steupUI{
    UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    bg.image = [UIImage imageNamed:@"sign_bg"];
    [self.view addSubview:bg];
    
    UIButton    *verifyButton = [[UIButton alloc]init];
    [verifyButton setTitle:@"验证码签到" forState:UIControlStateNormal];
    verifyButton.center = CGPointMake(WIDTH/2.0, HEIGHT/2.0);
    verifyButton.bounds = CGRectMake(0, 0, SIZE(600), 50);
    [verifyButton addTarget:self action:@selector(gesture:) forControlEvents:UIControlEventTouchUpInside];
    verifyButton.tag = 10;
    [verifyButton setBackgroundImage:[UIImage imageNamed:@"sign_btn0"] forState:UIControlStateNormal];
    [verifyButton setBackgroundImage:[UIImage imageNamed:@"sign_btn1"] forState:UIControlStateHighlighted];
    [self.view addSubview:verifyButton];
    
    UIButton    *faceButton = [[UIButton alloc]init];
    [faceButton setTitle:@"人脸签到" forState:UIControlStateNormal];
    faceButton.center = CGPointMake(WIDTH/2.0, HEIGHT/2.0-50-50);
    faceButton.bounds = verifyButton.bounds;
    faceButton.tag = 20;
    [faceButton addTarget:self action:@selector(gesture:) forControlEvents:UIControlEventTouchUpInside];
    [faceButton setBackgroundImage:[UIImage imageNamed:@"sign_btn0"] forState:UIControlStateNormal];
    [faceButton setBackgroundImage:[UIImage imageNamed:@"sign_btn1"] forState:UIControlStateHighlighted];
    [self.view addSubview:faceButton];
    
    UIButton    *QRButton = [[UIButton alloc]init];
    [QRButton setTitle:@"扫码签到" forState:UIControlStateNormal];
    QRButton.center = CGPointMake(WIDTH/2.0, HEIGHT/2.0+50+50);
    QRButton.bounds = verifyButton.bounds;
    QRButton.tag = 30;
    [QRButton addTarget:self action:@selector(gesture:) forControlEvents:UIControlEventTouchUpInside];
    [QRButton setBackgroundImage:[UIImage imageNamed:@"sign_btn0"] forState:UIControlStateNormal];
    [QRButton setBackgroundImage:[UIImage imageNamed:@"sign_btn1"] forState:UIControlStateHighlighted];
    [self.view addSubview:QRButton];

}
//三种识别方式
- (void)gesture:(UIButton   *)button
{
    QRViewController    *QRVC = [[QRViewController alloc]init];
    [self.navigationController pushViewController:QRVC animated:YES];
    return;
    [LYTools isLoginWithSuccess:^{
        if (button.tag == 10) {
            
        }else if (button.tag == 20){
            
        }else{
            QRViewController    *QRVC = [[QRViewController alloc]init];
            [self.navigationController pushViewController:QRVC animated:YES];
        }
    } failure:^{
        [MBProgressHUD showMessage:@"请先登录" complication:nil];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
