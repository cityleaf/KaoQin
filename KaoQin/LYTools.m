//
//  LYTools.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "LYTools.h"

@implementation LYTools
+ (NSMutableDictionary  *)getJsonFrompath:(NSString   *)str
{
    NSArray  *arr = [str componentsSeparatedByString:@"."];
    NSString    *path = [[NSBundle mainBundle]pathForResource:arr[0] ofType:arr[1]];
    NSLog(@"====%@",path);
    NSData  *data = [[NSData alloc]initWithContentsOfFile:path];

    NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"dic /////////%@",dic);
    return dic;
}

+ (void)isLoginWithSuccess:(void (^)())success failure:(void (^)())failure
{
    BOOL loginFlag = [USER_D boolForKey:@"LoginFlag"];
    
    if (loginFlag) {
        success();
    } else {
        failure();
    }
    
}

@end
