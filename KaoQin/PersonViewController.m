//
//  PersonViewController.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "PersonViewController.h"
#import "LoginViewController.h"
#import "LYTools.h"
#import "PersonCell.h"
#import "ExitCell.h"
#import "Masonry-master/Masonry/Masonry.h"
@interface PersonViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView     *_table;
    
}
@end

@implementation PersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBarHidden = YES;
//    
    
//    [self steupUI];
}
- (void)viewDidAppear:(BOOL)animated{
    [LYTools isLoginWithSuccess:^{
        [self steupUI];
    } failure:^{
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
    }];
}
- (void)steupUI
{
    _table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64) style:UITableViewStyleGrouped];
    _table.dataSource = self;
    _table.delegate = self;
    _table.separatorInset = UIEdgeInsetsMake(0, SIZE(160), 0, 0);
    _table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [_table registerNib:[UINib nibWithNibName:@"PersonCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [_table registerNib:[UINib nibWithNibName:@"ExitCell" bundle:nil] forCellReuseIdentifier:@"cell2"];
    [self.view addSubview:_table];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 5;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return SIZE(520);
    }
    return SIZE(10);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (UITableViewCell  *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        PersonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (indexPath.row == 0) {
            cell.titleLable.text = @"学校";
            cell.subLable.text = @"淮南师范学院";
        }else if (indexPath.row == 1){
            cell.titleLable.text = @"院系";
            cell.subLable.text = @"计算机学院";
        }else if (indexPath.row == 2){
            cell.titleLable.text = @"班级";
            cell.subLable.text = @"计算机科学与技术10(1)班";
        }else if (indexPath.row == 3){
            cell.titleLable.text = @"宿舍";
            cell.subLable.text = @"9B604";
        }else{
            cell.titleLable.text = @"学号";
            cell.subLable.text = @"1008010121";
        }
        return cell;
    }
    ExitCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2"];
    
    return cell;
}
- (UIView   *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView  *view = nil;
    if (section == 0) {
        view = [[UIView alloc]init];
        
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, SIZE(520))];
        bg.image = [UIImage imageNamed:@"person_bg"];
        [view addSubview:bg];
        UIVisualEffectView  *effectView = [[UIVisualEffectView alloc]initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
        effectView.alpha = 0.6;
        effectView.frame = CGRectMake(0, 0, WIDTH, SIZE(520));
        [view addSubview:effectView];
        
        UIImageView *head = [[UIImageView alloc]init];
        head.image = [UIImage imageNamed:@"head"];
        head.bounds = CGRectMake(0, 0, 100, 100);
        head.center = CGPointMake(WIDTH/2.0, 64+40);
        head.layer.cornerRadius = head.bounds.size.width/2.0;
        head.layer.masksToBounds = YES;
        [view addSubview:head];
        UILabel *name = [[UILabel alloc]init];
        name.bounds = CGRectMake(0, 0, WIDTH, 40);
        name.center = CGPointMake(WIDTH/2.0, head.center.y+head.bounds.size.height/2.0+30);
        name.text = @"刘宇";
        name.font = [UIFont systemFontOfSize:21];
        name.textAlignment = NSTextAlignmentCenter;
        [view addSubview:name];
        
        UILabel *phone = [[UILabel alloc]init];
        phone.bounds = CGRectMake(0, 0, WIDTH, 30);
        phone.center = CGPointMake(WIDTH/2.0, name.center.y+name.bounds.size.height/2.0+10);
        phone.textAlignment = NSTextAlignmentCenter;
        phone.text = @"18119548553";
        phone.textColor = [UIColor whiteColor];
        phone.font = [UIFont systemFontOfSize:16];
        [view addSubview:phone];
    }
    
    return view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1 && indexPath.row == 0) {
        [USER_D setBool:NO forKey:@"LoginFlag"];
        [USER_D synchronize];
        
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
