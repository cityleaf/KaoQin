//
//  ExitCell.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/5.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "ExitCell.h"
#import "Masonry.h"
@implementation ExitCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UILabel *lable = [[UILabel alloc]init];
    lable.text = @"退出登录";
    lable.textAlignment = NSTextAlignmentCenter;
    lable.textColor = [UIColor redColor];
    [self addSubview:lable];
    
    [lable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.and.bottom.and.right.equalTo(@0);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
