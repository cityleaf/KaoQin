//
//  RecoderModel.h
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecoderModel : NSObject
@property (nonatomic , copy) NSString   *type;
@property (nonatomic , copy) NSString   *className;
@property (nonatomic , copy) NSString   *time;
@property (nonatomic , copy) NSString   *collegeType;
+ (id)allocWithDic:(NSMutableDictionary *)dic;
@end
