//
//  LoginViewController.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "LoginViewController.h"
#import "Masonry-master/Masonry/Masonry.h"
#import "LYNetTools.h"
#import "MBProgressHUD+GR.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    UITextField     *_accountTF;
    UITextField     *_pswTF;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.93 alpha:1];
    [self steupUI];
}
- (void)steupUI{
    UIImageView *icon = [[UIImageView alloc]init];
    icon.bounds = CGRectMake(0, 0, 150, 50);
    icon.center = CGPointMake(WIDTH/2.0, 64+50);
    icon.image = [UIImage imageNamed:@"login_icon"];
    [self.view addSubview:icon];
    
    UIButton    *dismissButton = [[UIButton alloc]initWithFrame:CGRectMake(WIDTH-30-20, 30, 30, 30)];
    dismissButton.backgroundColor = [UIColor colorWithRed:0 green:0.73 blue:0.61 alpha:1];
    dismissButton.layer.cornerRadius = dismissButton.frame.size.width/2.0;
    dismissButton.layer.masksToBounds = YES;
    [dismissButton setTitle:@"取消" forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dismissButton];
    
    _accountTF = [[UITextField alloc]init];
    _accountTF.backgroundColor = [UIColor whiteColor];
    _accountTF.delegate = self;
    _accountTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self setTextFieldLeftPadding:_accountTF forWidth:20];
    _accountTF.placeholder = @"请输入学号/手机号";
    [self.view addSubview:_accountTF];
    
    _pswTF = [[UITextField alloc]init];
    _pswTF.backgroundColor = [UIColor whiteColor];
    _pswTF.delegate = self;
    _pswTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self setTextFieldLeftPadding:_pswTF forWidth:20];
    _pswTF.placeholder = @"亲输入密码";
    [self.view addSubview:_pswTF];
    
    UIButton    *loginBtn = [[UIButton alloc]init];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"login_btn"] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(requestForLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];

    [_accountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(icon.mas_bottom).offset(30);
        make.left.equalTo(@(30));
        make.right.equalTo(@(-30));
        make.height.equalTo(@(SIZE(88)));
    }];
    [_pswTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_accountTF.mas_bottom).offset(1);
        make.left.equalTo(_accountTF.mas_left);
        make.right.equalTo(_accountTF.mas_right);
        make.height.equalTo(_accountTF.mas_height);
    }];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_pswTF.mas_bottom).offset(40);
        make.left.equalTo(_pswTF.mas_left);
        make.right.equalTo(_pswTF.mas_right);
        make.height.equalTo(@(SIZE(86)));
    }];
    
}

- (void)requestForLogin{
    WS(weakSelf)
    NSString    *url = @"http://www.aishangke.net.cn/life/index.php/Home/Index/login";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    params[@"username"] = _accountTF.text;
    params[@"password"] = _pswTF.text;
    [LYNetTools post:url params:params success:^(id jsonObject) {
        
        NSDictionary *json = jsonObject;
        if ([json[@"success"] isEqualToString:@"success"]) {
            [MBProgressHUD showSuccess:json[@"message"] toView:weakSelf.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSLog(@"跳转");
                [USER_D setBool:YES forKey:@"LoginFlag"];
                [USER_D synchronize];
                
                [weakSelf dismiss];
                
            });
        }else{
            [MBProgressHUD showError:json[@"message"] toView:weakSelf.view];
        }

    } failure:^(NSError *error) {
        NSLog(@"error = %@",error);
        [MBProgressHUD showMessage:@"连接服务器失败"];
    }];
    
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

-(void)setTextFieldLeftPadding:(UITextField *)textField forWidth:(CGFloat)leftWidth
{
    CGRect frame = textField.frame;
    frame.size.width = leftWidth;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = leftview;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
