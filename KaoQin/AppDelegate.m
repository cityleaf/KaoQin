//
//  AppDelegate.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "AppDelegate.h"
#import "SignViewController.h"
#import "RecoderViewController.h"
#import "QingJiaViewController.h"
#import "PersonViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    UITabBarController *tab = [[UITabBarController alloc]init];
    tab.tabBar.tintColor = [UIColor colorWithRed:0.32 green:0.67 blue:0.9 alpha:1];
    /**
     *  签到      View
     */
    SignViewController *signVC = [[SignViewController alloc]init];
    
    signVC.view.backgroundColor = [UIColor redColor];
    UIImage *normalImg1 = [[UIImage imageNamed:@"tabbar_0"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImg1 = [[UIImage imageNamed:@"tabbar_00"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    signVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"签到" image:normalImg1 selectedImage:selectedImg1];
    signVC.title = @"签到";
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:signVC];
    [nav1.navigationBar setBarTintColor:[UIColor colorWithRed:0.32 green:0.67 blue:0.9 alpha:1]];
    [nav1.navigationBar setTintColor:[UIColor whiteColor]];
    [nav1.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    /**
     *  请假   View
     */
    QingJiaViewController *qingjiaVC = [[QingJiaViewController alloc]init];
    qingjiaVC.view.backgroundColor = [UIColor blueColor];
    UIImage *normalImg2 = [[UIImage imageNamed:@"tabbar_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImg2 = [[UIImage imageNamed:@"tabbar_11"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    qingjiaVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"请假" image:normalImg2 selectedImage:selectedImg2];
    qingjiaVC.title = @"请假";
    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:qingjiaVC];
    [nav2.navigationBar setBarTintColor:[UIColor colorWithRed:0.32 green:0.67 blue:0.9 alpha:1]];
    [nav2.navigationBar setTintColor:[UIColor whiteColor]];
    [nav2.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    /**
     *  出勤记录   View
     */
    RecoderViewController *chuqinVC = [[RecoderViewController alloc]init];
    chuqinVC.view.backgroundColor = [UIColor grayColor];
    UIImage *normalImg3 = [[UIImage imageNamed:@"tabbar_2"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImg3 = [[UIImage imageNamed:@"tabbar_22"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    chuqinVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"出勤记录" image:normalImg3 selectedImage:selectedImg3];
    chuqinVC.title = @"出勤记录";
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:chuqinVC];
    [nav3.navigationBar setBarTintColor:[UIColor colorWithRed:0.32 green:0.67 blue:0.9 alpha:1]];
    [nav3.navigationBar setTintColor:[UIColor whiteColor]];
//    nav3.navigationBarHidden = YES;
    [nav3.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    /**
     *  个人中心   View
     */
    PersonViewController *personViewController = [[PersonViewController alloc]init];
    personViewController.view.backgroundColor = [UIColor orangeColor];
    UIImage *normalrImg4 = [[UIImage imageNamed:@"tabbar_3"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImg4 = [[UIImage imageNamed:@"tabbar_33"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    personViewController.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"我" image:normalrImg4 selectedImage:selectedImg4];
    personViewController.title = @"我";
    UINavigationController *nav4 = [[UINavigationController alloc]initWithRootViewController:personViewController];
    [nav4.navigationBar setBarTintColor:[UIColor colorWithRed:0.32 green:0.67 blue:0.9 alpha:1]];
    [nav4.navigationBar setTintColor:[UIColor whiteColor]];
    [nav4.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    
    tab.viewControllers = @[nav1,nav3,nav4];
    self.window.rootViewController = tab;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
