//
//  RecoderModel.m
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import "RecoderModel.h"

@implementation RecoderModel
+ (id)allocWithDic:(NSMutableDictionary *)dic{
    return [[self alloc]initWithDic:dic];
}
- (id)initWithDic:(NSMutableDictionary  *)dic{
    self = [super init];
    if (self) {
        self.type = dic[@"type"];
        self.collegeType = dic[@"collegeType"];
        self.className = dic[@"className"];
        self.time = dic[@"time"];
    }
    return self;
}
@end
