//
//  RecordCell.h
//  KaoQin
//
//  Created by 刘宇 on 16/5/4.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecoderModel.h"
@interface RecordCell : UITableViewCell
@property (nonatomic , strong) UIImageView  *img;
@property (nonatomic , strong) UILabel  *titleLable;
@property (nonatomic , strong) UILabel  *subTitleLable;
@property (nonatomic , strong) UIButton *typeButton;
- (void)cellWithObject:(RecoderModel  *)object;
@end
