//
//  PersonCell.h
//  KaoQin
//
//  Created by 刘宇 on 16/5/5.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCell : UITableViewCell
@property (nonatomic , strong) UILabel  *titleLable;
@property (nonatomic , strong) UILabel  *subLable;
@end
