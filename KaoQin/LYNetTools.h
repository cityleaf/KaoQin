//
//  LYNetTools.h
//  KaoQin
//
//  Created by 刘宇 on 16/5/5.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@interface LYNetTools : NSObject
/**
 *  POST 请求
 *
 *  @param url     传入请求地址
 *  @param params  请求数据，为字典格式（json格式）
 *  @param success 请求成功调用的block
 *  @param failure 请求失败调用的block
 */
+ (void)post:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;

@end
