//
//  LYTabar.h
//  KaoQin
//
//  Created by 刘宇 on 16/5/3.
//  Copyright © 2016年 liuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LYTabar;
@protocol LYTabarDelegate <NSObject>
@optional
- (void)LYTabbar:(LYTabar   *)tabbar clickButton:(NSInteger)buttonIndex;

@end


@interface LYTabar : UITabBar
//- (id)initWithImg:(NSArray <UIImage *>*)imgs titles:(NSArray <NSString   *>*)title;
@property (nonatomic , assign) id<LYTabarDelegate>  LYDelegate;
@end
